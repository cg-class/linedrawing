#include "dda.h"
#include <algorithm>
#include <cmath>

std::vector<std::pair<int, int>> dda_simple(std::pair<int, int> const &start,
                                            std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int e = std::abs(dx) < std::abs(dy) ? std::abs(dy) : std::abs(dx);
  double incX = static_cast<double>(dx) / e;
  double incY = static_cast<double>(dy) / e;
  std::vector<std::pair<int, int>> res;
  res.reserve(e);
  res.push_back(start);
  double x = start.first;
  double y = start.second;
  for (int i = 0; i < e; ++i) {
    x += incX;
    y += incY;
    res.push_back(
        {static_cast<int>(std::round(x)), static_cast<int>(std::round(y))});
  }
  return res;
}

std::vector<std::pair<int, int>> dda_sym(std::pair<int, int> const &start,
                                         std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int e = std::abs(dx) < std::abs(dy) ? std::abs(dy) : std::abs(dx);
  e = 1 << static_cast<int>(std::floor(log2(e)) + 1);
  double incX = static_cast<double>(dx) / e;
  double incY = static_cast<double>(dy) / e;
  std::vector<std::pair<int, int>> res;
  res.reserve(e);
  res.push_back(start);
  double x = start.first;
  double y = start.second;
  for (int i = 0; i < e; ++i) {
    x += incX;
    y += incY;
    res.push_back(
        {static_cast<int>(std::round(x)), static_cast<int>(std::round(y))});
  }
  return res;
}
