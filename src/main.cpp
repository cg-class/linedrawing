#include <GL/glew.h>

#include <GL/freeglut.h>
#include <algorithm>
#include <array>
#include <bitset>
#include <cmath>
#include <fmt/core.h>
#include <iostream>
#include <sstream>
#include <string>

#include "bresenham.h"
#include "circle.hpp"
#include "dda.h"
#include "ellipse.hpp"
#include "fill.hpp"

#ifdef LINE
const std::array names = {"DDA Simple", "DDA Sym", "Bresenham", "MidPoint"};
const std::array funcs = {dda_simple, dda_sym, bresenham, bresenham};
#endif
#ifdef CIRCLE
const std::array names = {"MidPoint Circle"};
const std::array funcs = {midcircle};
#endif
#ifdef ELLIPSE
const std::array names = {"MidPoint Ellipse"};
const std::array funcs = {midellipse};
#endif
enum struct KeyState_ {
  PATTERN = 'p',
  RGB = 'r',
  NORMAL = 'n',
  INPUT = 'i',
};
KeyState_ state = KeyState_::NORMAL;

std::vector<int> windows;
const int winX = 500, winY = 500;

std::vector<std::pair<int, int>> vertices = {};
std::pair<std::pair<int, int>, std::pair<int, int>> clippedRect;
std::stringstream input_buffer;
std::bitset<16> pattern = 0xffff;
float pointSize = 2;
std::array<short, 3> rgb = {255, 255, 255};
int pattCount = 0;
#if defined(CIRCLE) || defined(ELLIPSE)
bool fill = false;
#endif

// populates vertices to draw
auto raster = [old = std::make_pair(0, 0),
               latch = 0](std::pair<int, int> const &point,
                          auto *_Func) mutable {
  if (latch % 2) {
    #if defined (CIRCLE) || defined(ELLIPSE)
    vertices = _Func(old, point, fill);
    #else
    vertices = _Func(old, point);
    #endif
    glutPostRedisplay();
  } else {
    old = point;
  }
  ++latch;
};

void display() {
  glColor3f(1, 1, 1);
  glRasterPos2i(winX - 400, winY - 50);
  glutBitmapString(
      GLUT_BITMAP_HELVETICA_18,
      reinterpret_cast<const unsigned char *>(
          fmt::format("R: {}, G: {}, B: {}", rgb[0], rgb[1], rgb[2]).c_str()));
  glutBitmapString(GLUT_BITMAP_HELVETICA_18,
                   reinterpret_cast<const unsigned char *>(
                       fmt::format("  {}", pattern.to_string()).c_str()));
  glColor3f(rgb[0] / 255., rgb[1] / 255., rgb[2] / 255.);
  glPointSize(pointSize);
  glBegin(GL_POINTS);
  int i = 0;
  for (auto const &iter : vertices) {
    if (pattern[i % 8]) {
      glVertex2i(iter.first, iter.second);
    }
    ++i;
  }
  vertices.clear();
  glEnd();
  glutSwapBuffers();
}

void glInit() {
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, winX, winY, 0, 0, 1.0);
}

template <typename _T> void mouse(int button, int state, int x, int y) {
  switch (button) {
  case GLUT_LEFT_BUTTON:
    if (state == GLUT_DOWN) {
      glutSetWindow(windows[_T::value]);
      raster({x, y}, funcs[_T::value]);
    }
    break;
  }
}

template <typename _T> void handle_exit() {
  switch (state) {
  case KeyState_::INPUT: {
    int a, b;
    input_buffer >> a >> b;
    raster({a, b}, funcs[_T::value]);
    input_buffer >> a >> b;
    raster({a, b}, funcs[_T::value]);
    break;
  }
  case KeyState_::PATTERN: {
    std::string s;
    input_buffer >> s;
    if (s.size() > 16)
      s.erase(17, std::string::npos);
    pattern = decltype(pattern)(s);
    break;
  }
  case KeyState_::RGB: {
    short r, g, b;
    input_buffer >> r >> g >> b;
    rgb = {r, g, b};
    glutPostRedisplay();
    break;
  }
  case KeyState_::NORMAL:
    break;
  }
  input_buffer.clear();
}

template <typename _T> void handle_input(unsigned char const &key) {
  switch (key) {
  case 13:
    std::cout << '\n';
    input_buffer.put('\n');
    break;
  case 8: {
    std::cout << "\033[D\033[K";
    input_buffer.get();
    break;
  }
  default: {
    std::cout << key << std::flush;
    input_buffer << key;
    break;
  }
  }
  std::cout.flush();
}

template <typename _T> void keyboard(unsigned char key, int x, int y) {
  glutSetWindow(windows[_T::value]);
  // std::cout << "Key: " << static_cast<short>(key) << std::endl;
  if (key == '\E') {
    std::cout << "Normal Mode" << std::endl;
    handle_exit<_T>();
    state = KeyState_::NORMAL;
    return;
  }
  if (state != KeyState_::NORMAL) {
    handle_input<_T>(key);
  }
  switch (key) {
  case 'p':
    state = KeyState_::PATTERN;
    std::cout << "Pattern Mode" << std::endl;
    pattern = 0;
    break;
  case 'r':
    state = KeyState_::RGB;
    std::cout << "RGB Mode" << std::endl;
    rgb = {0, 0, 0};
    break;
#if defined(CIRCLE) || defined(ELLIPSE)
  case 'f':
    std::cout << "Toggle Fill Mode" << std::endl;
    fill = !fill;
    break;
#endif
  case 'i':
    state = KeyState_::INPUT;
    std::cout << "Input Data" << std::endl;
    break;
  }
}

template <int N> struct WinGen {
  enum { value = N - 1 };
  static void gen() {
    WinGen<N - 1>::gen();
    int win = glutCreateWindow(names[value]);
    glutSetWindow(win);
    windows.push_back(win);
    glutDisplayFunc(display);
    glutMouseFunc(mouse<WinGen<N>>);
    glutKeyboardUpFunc(keyboard<WinGen<N>>);
    glInit();
  }
};
template <> struct WinGen<0> {
  static void gen() { return; }
};

int main(int argc, char **args) {
  glutInit(&argc, args);
  glutInitDisplayMode(GLUT_DOUBLE);
  glutInitWindowSize(winX, winY);
  glutInitWindowPosition(0, 0);

  WinGen<names.size()>::gen();

  std::cerr << glGetString(GL_VERSION) << std::endl;
  glutMainLoop();
  return 0;
}
