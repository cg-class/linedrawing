#pragma once
#include <array>

void boundary_fill(int x, int y, std::array<short, 3> fill_c,
                   std::array<short, 3> boundary_c);
void flood_fill(int x, int y, std::array<short, 3> fill_c,
                   std::array<short, 3> boundary_c);
