#include "circle.hpp"
#include "bresenham.h"
#include <cmath>

std::vector<std::pair<int, int>> midcircle_non(std::pair<int, int> const &center,
                                           std::pair<int, int> const &end) {
  std::vector<std::pair<int, int>> res;
  std::pair<int, int> r2 = {std::pow(end.first - center.first, 2),
                            std::pow(end.second - center.second, 2)};
  double r = std::sqrt(static_cast<double>(r2.first + r2.second));
  double p0, p;
  double x = 0.0, y = r;
  p0 = 1 - r;
  while (x <= y) {

    double xn = 0.0 - x, yn = 0.0 - y;
    res.emplace_back(x + center.first, y + center.second);
    res.emplace_back(xn + center.first, yn + center.second);
    res.emplace_back(y + center.first, x + center.second);
    res.emplace_back(yn + center.first, xn + center.second);
    res.emplace_back(y + center.first, xn + center.second);
    res.emplace_back(x + center.first, yn + center.second);
    res.emplace_back(yn + center.first, x + center.second);
    res.emplace_back(xn + center.first, y + center.second);
    if (p0 >= 0) {
      x = x + 1;
      y = y - 1;
      p = p0 + 2 * (x - y) + 1;
    } else {
      x = x + 1;
      p = p0 + 2 * x + 1;
    }
    p0 = p;
  }
  return res;
}
std::vector<std::pair<int, int>>
midcircle_fill(std::pair<int, int> const &center,
               std::pair<int, int> const &end) {
  std::vector<std::pair<int, int>> res;
  std::pair<int, int> r2 = {std::pow(end.first - center.first, 2),
                            std::pow(end.second - center.second, 2)};
  double r = std::sqrt(static_cast<double>(r2.first + r2.second));
  double p0, p;
  double x = 0.0, y = r;
  p0 = 1 - r;
  while (x <= y) {

    double xn = 0.0 - x, yn = 0.0 - y;
    auto tmp = bresenham({x + center.first, y + center.second},
                         {xn + center.first, yn + center.second});
    res.insert(res.end(), tmp.begin(), tmp.end());
    tmp = bresenham({y + center.first, x + center.second},
                    {yn + center.first, xn + center.second});
    res.insert(res.end(), tmp.begin(), tmp.end());
    tmp = bresenham({y + center.first, xn + center.second},
                    {yn + center.first, x + center.second});
    res.insert(res.end(), tmp.begin(), tmp.end());
    tmp = bresenham({x + center.first, yn + center.second},
                    {xn + center.first, y + center.second});
    res.insert(res.end(), tmp.begin(), tmp.end());
    if (p0 >= 0) {
      x = x + 1;
      y = y - 1;
      p = p0 + 2 * (x - y) + 1;
    } else {
      x = x + 1;
      p = p0 + 2 * x + 1;
    }
    p0 = p;
  }
  return res;
}

std::vector<std::pair<int, int>> midcircle(std::pair<int, int> const &center,
                                           std::pair<int, int> const &end, bool fill) {
  if(fill){
    return midcircle_fill(center, end);
  }
  return midcircle_non(center, end);
}

