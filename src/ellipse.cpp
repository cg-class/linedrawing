#include <algorithm>
#include <vector>
using point_t = std::pair<int, int>;
std::vector<point_t> midellipse(point_t const &center,
                                point_t const &semi_axes, bool fill) {
  std::vector<point_t> trajectory_points;
  std::ptrdiff_t x = semi_axes.first, y = 0;

  long long int const t1 = semi_axes.first * semi_axes.first;
  long long int const t4 = semi_axes.second * semi_axes.second;
  long long int t2, t3, t5, t6, t8, t9;
  t2 = 2 * t1, t3 = 2 * t2;
  t5 = 2 * t4, t6 = 2 * t5;
  long long int const t7 = semi_axes.first * t5;
  t8 = 2 * t7, t9 = 0;

  long long int d1, d2;
  d1 = t2 - t7 + t4 / 2, d2 = t1 / 2 - t8 + t5;

  while (d2 < 0) {
    trajectory_points.push_back({x, y});
    y += 1;
    t9 += t3;
    if (d1 < 0) {
      d1 += t9 + t2;
      d2 += t9;
    } else {
      x -= 1;
      t8 -= t6;
      d1 += t9 + t2 - t8;
      d2 += t5 + t9 - t8;
    }
  }
  while (x >= 0) {
    trajectory_points.push_back({x, y});
    x -= 1;
    t8 -= t6;
    if (d2 < 0) {
      y += 1;
      t9 += t3;
      d2 += t5 + t9 - t8;
    } else {
      d2 += t5 - t8;
    }
  }
  std::vector<point_t> pts;
  std::for_each(trajectory_points.begin(), trajectory_points.end(),
                [&pts](auto &p) {
                  if (p.first != 0)
                    pts.push_back({p.first * -1, p.second});
                  if (p.second != 0)
                    pts.push_back({p.first, p.second * -1});
                  if (p.first != 0 && p.second != 0)
                    pts.push_back({p.first * -1, p.second * -1});
                });
  trajectory_points.insert(trajectory_points.end(), pts.begin(), pts.end());
  std::for_each(trajectory_points.begin(), trajectory_points.end(),
                [&center](auto &p) {
                  p.first += center.first;
                  p.second += center.second;
                });

  return trajectory_points;
}
