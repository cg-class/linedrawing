#pragma once
#include <vector>
using point_t = std::pair<int, int>;
std::vector<std::pair<int, int>> midellipse(std::pair<int, int> const &center,
                       std::pair<int, int> const &semi_axes, bool fill);
