#pragma once
#include <vector>

std::vector<std::pair<int, int>> midcircle(std::pair<int, int> const &center,
                           std::pair<int, int> const &end, bool fill);
