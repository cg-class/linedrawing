#pragma once
#include <vector>


std::vector<std::pair<int, int>> dda_simple(std::pair<int, int> const &start,
                           std::pair<int, int> const &end);
std::vector<std::pair<int, int>> dda_sym(std::pair<int, int> const &start,
                           std::pair<int, int> const &end);
